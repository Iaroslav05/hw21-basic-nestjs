import { Module } from '@nestjs/common';
import { HashService } from './hash.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Record } from '../record/entities/record.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Record])],
  providers: [HashService],
})
export class CommonModule {}
