import { IsNotEmpty, IsString } from 'class-validator';
export class CreateRecordDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  content: string;
}
