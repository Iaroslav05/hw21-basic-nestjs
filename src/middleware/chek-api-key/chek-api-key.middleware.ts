import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class ChekApiKeyMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    const apiKey = req.headers['api-key'];

    if (!apiKey) {
      return res.status(403).json({
        message: 'API key not found',
      });
    }

    if (apiKey !== process.env.API_KEY) {
      return res.status(403).json({
        message: 'API key is not valid',
      });
    }
    next();
  }
}
