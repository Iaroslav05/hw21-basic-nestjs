import { ChekApiKeyMiddleware } from './chek-api-key.middleware';

describe('ChekApiKeyMiddleware', () => {
  it('should be defined', () => {
    expect(new ChekApiKeyMiddleware()).toBeDefined();
  });
});
