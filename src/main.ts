import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ChekApiKeyMiddleware } from './middleware/chek-api-key/chek-api-key.middleware';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(new ChekApiKeyMiddleware().use);
  app.setGlobalPrefix('api');
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
